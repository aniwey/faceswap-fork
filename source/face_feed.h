#include <vector>
#include <string>

extern std::string source_folder;

// Find usable faces
class face_feed
{
public:
	face_feed();
	static face_feed * inst;
	std::string GetRandomFacePath();

	
private:
	std::vector<std::string> m_file_list;

	const char * faces_folder = (source_folder + "../photo_heads/").c_str();

	std::vector<std::string> generate_filelist(const char* faces_folder);
};
