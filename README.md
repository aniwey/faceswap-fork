Fork of https://github.com/EmileSonneveld/FaceSwap

Fixed the --dest parameter so that it works correctly

Added a --sourceFolder parameter to specify the project source folder, therefore allowing to launch the binary from another working directory